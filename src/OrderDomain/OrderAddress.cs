﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrderDomain
{
    public class OrderAddress
    {
        public Guid AddressId { get; set; }
        public string EirCode { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrderDomain
{
    public class OrderItem
    {
        public Guid OrderItemId {get;set;}
        public int Quantity { get; set; }
    }
}

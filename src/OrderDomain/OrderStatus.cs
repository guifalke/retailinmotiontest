﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrderDomain
{
    public enum OrderStatus:int
    {
        Incompleted =1,
        Recived = 2,
        Altered = 3,
        IncompletedUpdate = 4,
        Deleted = 5,
        ProcessingDeletion = 6,
        Error = 7,
    }
}

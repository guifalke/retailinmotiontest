﻿using System;
using System.Collections.Generic;

namespace OrderDomain
{
    public class Order
    {
        public Guid OrderId { get; set; }
        public int Status { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }

        public OrderAddress Address {get;set;}

        public List<OrderItem> Itens{get;set;}
    }
}

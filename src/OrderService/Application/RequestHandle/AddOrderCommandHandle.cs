﻿using MediatR;
using MessageQueue;
using OrderDB;
using OrderDomain;
using OrderService.Application.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace OrderService.Application.RequestPipeline
{
     
    public class AddOrderCommandHandle : IRequestHandler<AddOrderCommand,Order>
    {
        private readonly IBackgroundTaskQueue _taskQueue;
        private readonly IOrderRepository _orderRepository;

        public AddOrderCommandHandle(IBackgroundTaskQueue taskQueue, IOrderRepository orderRepository)
        {
            _taskQueue = taskQueue;
            _orderRepository = orderRepository;
        }

        public async Task<Order> Handle(AddOrderCommand request, CancellationToken cancellationToken)
        {   
            request.Address.AddressId = Guid.NewGuid();
            for(int g=0;g<request.Itens.Count(); g++)
            {
                request.Itens[g].OrderItemId = Guid.NewGuid();
            }
            var toInserted = new Order()
            {
                OrderId = Guid.NewGuid(),
                Status = (int)OrderStatus.Incompleted,
                CreatedOn = DateTime.Now,
                UpdatedOn = DateTime.Now,
                Address = request.Address,
                Itens = request.Itens
            };
            _taskQueue.QueueBackgroundWorkItem(_orderRepository.Add(
               toInserted
             ));
            return toInserted;
        }
    }
}

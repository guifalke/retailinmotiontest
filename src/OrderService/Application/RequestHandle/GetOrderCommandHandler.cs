﻿using MediatR;
using OrderDB;
using OrderDomain;
using OrderService.Application.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace OrderService.Application.RequestHandle
{
    public class GetOrderCommandHandler : IRequestHandler<GetOrderCommand, IEnumerable<Order>>
    {
        private readonly IOrderRepository _orderRepository;

        public GetOrderCommandHandler(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public async Task<IEnumerable<Order>> Handle(GetOrderCommand request, CancellationToken cancellationToken)
        {
            if (request.QuantityPerPage == 0)
                request.QuantityPerPage = 10;
            if (!string.IsNullOrEmpty(request.OrderId))
            {
                return await _orderRepository.GetById(new Guid(request.OrderId));
            }
            return await _orderRepository.Get(request.CurrentPage,request.QuantityPerPage);
        }
    }
}

﻿using MediatR;
using MessageQueue;
using OrderDB;
using OrderDomain;
using OrderService.Application.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace OrderService.Application.RequestHandle
{
    public class UpdateOrderCommandHandle : IRequestHandler<UpdateOrderCommand, Order>
    {
        private readonly IBackgroundTaskQueue _taskQueue;
        private readonly IOrderRepository _orderRepository;

        public UpdateOrderCommandHandle(IBackgroundTaskQueue taskQueue, IOrderRepository orderRepository)
        {
            _taskQueue = taskQueue;
            _orderRepository = orderRepository;
        }

        public async Task<Order> Handle(UpdateOrderCommand request, CancellationToken cancellationToken)
        {
            Order alteredOrder = new Order()
            {
                OrderId = request.OrderId,
                Status = (int)OrderStatus.IncompletedUpdate,
                UpdatedOn = DateTime.Now,
                Address = request.Address,
                Itens = request.Itens
            };
            _taskQueue.QueueBackgroundWorkItem(_orderRepository.Update(
               alteredOrder
            ));
            return alteredOrder;
        }
    }
}

﻿using MediatR;
using MessageQueue;
using OrderDB;
using OrderDomain;
using OrderService.Application.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace OrderService.Application.RequestHandle
{
    public class DeleteOrderCommandHandle : IRequestHandler<DeleteOrderCommand>
    {
        private readonly IBackgroundTaskQueue _taskQueue;
        private readonly IOrderRepository _orderRepository;

        public DeleteOrderCommandHandle(IBackgroundTaskQueue taskQueue, IOrderRepository orderRepository)
        {
            _taskQueue = taskQueue;
            _orderRepository = orderRepository;
        }

        public async Task<Unit> Handle(DeleteOrderCommand request, CancellationToken cancellationToken)
        {
            _taskQueue.QueueBackgroundWorkItem(_orderRepository.Delete(
                new Order()
                {
                    OrderId = request.OrderId,
                    Status = (int)OrderStatus.ProcessingDeletion
                }
             ));
            return Unit.Value;
        }
    }
}

﻿using MediatR;
using OrderDomain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderService.Application.Command
{
    public class GetOrderCommand : IRequest<IEnumerable<Order>>
    {
        public string OrderId {get;set;}
        public int QuantityPerPage { get; set; }
        public int CurrentPage { get; set; }
    }
}

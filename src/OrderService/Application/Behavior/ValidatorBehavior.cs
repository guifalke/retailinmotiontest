﻿using FluentValidation;
using FluentValidation.Results;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace OrderService.Application.Behavior
{
    public class ValidatorBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    {
        private readonly IEnumerable<IValidator<TRequest>> _validators;
        public ValidatorBehavior(IEnumerable<IValidator<TRequest>> validators) => _validators = validators;       

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            IEnumerable<ValidationResult> vr = await Task.WhenAll(_validators.Select(async v => await v.ValidateAsync(request)));
            var failures = vr.SelectMany(result => result.Errors)
                .Where(error => error != null)
                .ToList();
            if (failures.Any())
            {
                throw new ValidationException("Validation exception", failures);
            }
            var response = await next();
            return response;
        }
    }
}

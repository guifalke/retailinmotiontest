﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using OrderService.Application.Behavior;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OrderService.Application.Behavior;

namespace OrderService.Application.Config
{
    public static class ConfigMediaR
    {
        public static IServiceCollection AddMediatRPipelineBehaviors(this IServiceCollection services)
        {
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidatorBehavior<,>));
            return services;
        }
    }
}

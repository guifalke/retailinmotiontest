﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OrderDomain;
using OrderService.Application.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OrderController : ControllerBase
    {

        private readonly ILogger<OrderController> _logger;
        private readonly IMediator _mediator;

        public OrderController(ILogger<OrderController> logger, IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<IActionResult> Add([FromBody] AddOrderCommand newOrder)
        {
            
            return Ok(await _mediator.Send(newOrder));
        }
        [HttpPut]
        public async Task<IActionResult> Update([FromBody] UpdateOrderCommand newOrder)
        {            
            return Ok(await _mediator.Send(newOrder));
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery]GetOrderCommand getQuery){
            var returnable = await _mediator.Send(getQuery);
            if (returnable.Count() == 1)
            {
                return Ok(returnable.FirstOrDefault());
            }
            return Ok(returnable);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete([FromQuery] DeleteOrderCommand getQuery)
        {
            await _mediator.Send(getQuery);
            return Ok();
        }
    }
}

﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace MessageQueue
{
    public interface IBackgroundTaskQueue
    {
        void QueueBackgroundWorkItem(Task workItem);

        Task<Task> DequeueAsync(CancellationToken cancellationToken);
    }
}

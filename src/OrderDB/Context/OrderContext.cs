using Microsoft.EntityFrameworkCore;
using OrderDomain;

namespace OrderDB
{
    public class OrderContext : DbContext
    {
        public OrderContext(DbContextOptions<OrderContext> options):base(options) { }
        public DbSet<Order> Orders {get;set;}
        public DbSet<OrderAddress> OrderAddresses { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Order>()
                .HasKey(o => o.OrderId);

            builder.Entity<Order>()
                .HasOne(o => o.Address);

            builder.Entity<Order>()
                .HasMany(o => o.Itens);

            builder.Entity<OrderAddress>().HasKey(a => a.AddressId);

            builder.Entity<OrderItem>().HasKey(i=>i.OrderItemId);


        }
    }
}
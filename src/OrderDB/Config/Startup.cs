using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Sqlite;
using Microsoft.Extensions.DependencyInjection;
using OrderDB.Repository;

namespace OrderDB.Config
{
    public static class Startup
    {
        public static IServiceCollection AddDatabase(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<OrderContext>(options => options.UseSqlite(connectionString));
            services.AddScoped<IOrderRepository, OrderRepository>();
            return services;
        }
    }
}
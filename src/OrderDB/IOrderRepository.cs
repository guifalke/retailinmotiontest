﻿using OrderDomain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderDB
{
    public interface IOrderRepository
    {
        Task Add(Order order);
        Task<IQueryable<Order>> Get(int currentPage, int quantityPerPage);
        Task<IQueryable<Order>> GetById(Guid ProductId);
        Task Update(Order order);
        Task Delete(Order order);
    }
}

﻿using Microsoft.EntityFrameworkCore;
using OrderDomain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderDB.Repository
{
    class OrderRepository: IOrderRepository
    {
        private readonly OrderContext _context;

        public OrderRepository(OrderContext context)
        {
            _context = context;
        }

        public async Task Add(Order order)
        {
            order.Status = (int)OrderStatus.Recived;
            await _context.AddAsync(order);
            await _context.SaveChangesAsync();
        }
        public async Task Update(Order order)
        {
            var toUpdate = (await GetById(order.OrderId)).FirstOrDefault<Order>();
            toUpdate.Address = order.Address;
            toUpdate.Itens = order.Itens;
            if(toUpdate.Status != (int)OrderStatus.Deleted)
                toUpdate.Status = (int)OrderStatus.Altered;
            _context.Update(toUpdate);
            await _context.SaveChangesAsync();
        }
        public async Task<IQueryable<Order>> Get(int currentPage, int quantityPerPage)
        {
            return _context.Orders
                .Include(g=>g.Address)
                .Include(u=>u.Itens)
                .Where(i=>i.Status != (int)OrderStatus.Deleted)
                .Skip(currentPage* quantityPerPage)
                .Take(quantityPerPage);
        }
        public async Task<IQueryable<Order>> GetById(Guid ProductId)
        {
            return _context.Orders
                .AsNoTracking()
                .Include(g=>g.Address)
                .Include(u => u.Itens)
                .Where(i => i.OrderId == ProductId && i.Status != (int)OrderStatus.Deleted)
                .OrderBy(l=>l.CreatedOn).ThenBy(h=>h.UpdatedOn);
                
        }

        public async Task Delete(Order order)
        {
            var toDelete = (await GetById(order.OrderId)).FirstOrDefault<Order>();
            toDelete.Status = (int)OrderStatus.Deleted;
            _context.Update(toDelete);
            await _context.SaveChangesAsync();
        }
    }
}
